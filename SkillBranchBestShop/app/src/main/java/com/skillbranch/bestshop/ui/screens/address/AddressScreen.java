package com.skillbranch.bestshop.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MenuItem;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import com.skillbranch.bestshop.R;
import com.skillbranch.bestshop.data.storage.dto.UserAddressDto;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.di.scopes.AddressScope;
import com.skillbranch.bestshop.flow.AbstractScreen;
import com.skillbranch.bestshop.flow.Screen;
import com.skillbranch.bestshop.mvp.models.AccountModel;
import com.skillbranch.bestshop.mvp.presenters.AbstractPresenter;
import com.skillbranch.bestshop.mvp.presenters.IAddressPresenter;
import com.skillbranch.bestshop.mvp.presenters.MenuItemHolder;
import com.skillbranch.bestshop.ui.screens.account.AccountScreen;
import mortar.MortarScope;

@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey {
    @Nullable
    private UserAddressDto mAddressDto;

    public AddressScreen(@Nullable UserAddressDto mAddressDto) {
        this.mAddressDto = mAddressDto;
    }

    @Override
    public boolean equals(Object o) {
        if(mAddressDto != null) {
            return o instanceof AddressScreen && mAddressDto.equals(((AddressScreen) o).mAddressDto);
        }
        return super.equals(o);
    }


    @Override
    public int hashCode() {
        return mAddressDto != null ? mAddressDto.hashCode() : super.hashCode();
    }

    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder()
                .component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    //region ===================== DI =====================

    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @AddressScope
    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    public interface Component {
        void inject(AddressPresenter presenter);

        void inject(AddressView view);
    }

    //endregion

    //region ===================== Presenter =====================

    public class AddressPresenter extends AbstractPresenter<AddressView, AccountModel> implements IAddressPresenter {

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if(mAddressDto != null && getView() != null) {
                getView().initView(mAddressDto);
            }
        }

        @Override
        protected void initActionBar() {
            String title = "Добавление адреса";
            if(mAddressDto != null) {
                title = "Редактирование адреса";
            }

            mRootPresenter.newActionBarBuilder()
                    .setTitle(title)
                    .setBackArrow(true)
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void clickOnAddAddress() {
            // TODO: 28.11.2016 save address in model
            if (getView() != null) {
                mModel.updateOrInsertAddress(getView().getUserAddress());
                goBackToParentScreen();
            }
        }

        @Override
        public void goBackToParentScreen() {
            Flow.get(getView()).goBack();
        }

//        @Nullable
//        IRootView getRootView() {
//            return mRootPresenter.getView();
//        }
    }

    //endregion
}
