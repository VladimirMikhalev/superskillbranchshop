package com.skillbranch.bestshop.mvp.models;

public interface IAuthModel {
    boolean isAuthUser();
    void saveAuthToken(String authToken);
}
