package com.skillbranch.bestshop.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);
    boolean checkUserAuth();
}
