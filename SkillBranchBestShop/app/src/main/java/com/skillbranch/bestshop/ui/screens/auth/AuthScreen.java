package com.skillbranch.bestshop.ui.screens.auth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.skillbranch.bestshop.R;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.di.scopes.DaggerScope;
import com.skillbranch.bestshop.flow.AbstractScreen;
import com.skillbranch.bestshop.flow.Screen;
import com.skillbranch.bestshop.mvp.models.AuthModel;
import com.skillbranch.bestshop.mvp.models.IAuthModel;
import com.skillbranch.bestshop.mvp.presenters.IAuthPresenter;
import com.skillbranch.bestshop.mvp.presenters.RootPresenter;
import com.skillbranch.bestshop.mvp.views.IRootView;
import com.skillbranch.bestshop.ui.activities.RootActivity;
import com.skillbranch.bestshop.ui.activities.SplashActivity;
import com.skillbranch.bestshop.utils.ConstantManager;
import com.skillbranch.bestshop.utils.NetworkStatusChecker;

import javax.inject.Inject;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

@Screen(R.layout.screen_auth)
public class AuthScreen extends AbstractScreen<RootActivity.RootComponent> {
    private boolean mIsCatalogLoading=false;
    private int mCustomState = 1;
    private String screen;

    public AuthScreen(String screen) {
        this.screen = screen;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
//        return null;
        return DaggerAuthScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int mCustomState) {
        this.mCustomState = mCustomState;
    }

    //region ============================= DI =============================
    @dagger.Module
    public class Module {
        @Provides
        @DaggerScope(AuthScreen.class)
        AuthScreen.AuthPresenter provideAuthPresenter() {
            return new AuthScreen.AuthPresenter(screen);
        }

        @Provides
        @DaggerScope(AuthScreen.class)
        IAuthModel provideAuthModel() {
            return new AuthModel();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(AuthScreen.class)
    public interface Component {
        void inject(AuthPresenter authPresenter);
        void inject(AuthView authView);
    }

    //endregion

    //region ============================= Presenter =============================

    public class AuthPresenter extends ViewPresenter<AuthView> implements IAuthPresenter {
        @Inject
        RootPresenter mRootPresenter;
        @Inject
        IAuthModel mAuthModel;

        private String mScreen;

        public AuthPresenter(String screen) {
            mScreen = screen;
        }


        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            if(mScreen.contains("Catalog")) {
                mRootPresenter.newActionBarBuilder()
                        .setTitle("Авторизация")
                        .setBackArrow(true)
                        .build();
            }


            if(getView() != null) {
                if(checkUserAuth() && getRootView() != null) {
                    getView().hideLoginBtn();
                } else {
                    getView().showLoginBtn();
                }
            }
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        //        @Override
//        protected void initActionBar() {
//
//        }
//
//        @Override
//        protected void initDagger(MortarScope scope) {
//            ((Component)scope.getService(DaggerService.SERVICE_NAME)).inject(this);
//        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getRootView();
        }

        @Override
        public void clickOnLogin() {
            if (getView()!=null && getRootView() != null) {
                if (getView().isIdle()) {
                    getView().showLoginWithAnim();
                } else {
                    boolean emailOk = getView().getEmail().matches(ConstantManager.PATTERN_EMAIL);
                    boolean passwordOk = getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD);
                    if (emailOk && passwordOk) {
                        getRootView().showMessage(getView().getContext().getString(R.string.user_authenticating_message));
                        loginUser(getView().getUserEmail(), getView().getUserPassword());
//                    mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(), getView().getAuthPanel().getUserPassword());
                    } else {
                        getView().errorAuthForm();
                        if (!emailOk) getView().setWrongEmailError();
                        if (!passwordOk) getView().setWrongPasswordError();
                        getRootView().showMessage(getView().getContext().getString(R.string.email_or_password_wrong_format));
                    }
                }
            }
        }

        private void loginUser(String userEmail, String userPassword) {
            if (NetworkStatusChecker.isNetworkAvailable()) {
                mAuthModel.saveAuthToken("authenticated");
                onLoginSuccess();
            } else {
                onLoginError(String.valueOf(String.valueOf(R.string.error_network_failure)));
            }
        }

        @Override
        public void clickOnFb() {
            if (getRootView()!=null) {
                getRootView().showMessage("clickOnFb");
            }
        }

        @Override
        public void clickOnVk() {
            if (getRootView()!=null) {
                getRootView().showMessage("clickOnVk");
            }
        }

        @Override
        public void clickOnTwitter() {
            if (getRootView()!=null) {
                getRootView().showMessage("clickOnTwitter");
            }
        }

        @Override
        public void clickOnShowCatalog() {
            mIsCatalogLoading = true;
            if (getView() != null && getRootView()!=null) {
                getRootView().showMessage(getView().getContext().getString(R.string.catalog_loading_message));
                getRootView().showLoad();
                class WaitSplash extends AsyncTask<Void, Void, Void> {
                    protected Void doInBackground(Void... params) {
                        try {
                            Thread.currentThread();
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                    protected void onPostExecute(Void result) {
                        super.onPostExecute(result);
                        mIsCatalogLoading = false;
                        if (getView()!=null) {
                            getRootView().hideLoad();
                            getRootView().showMessage(getView().getContext().getString(R.string.catalog_loaded_message));
                            if(getRootView() instanceof RootActivity) {
                                ((RootActivity) getRootView()).onBackPressed();
                            }
                        }
                    }
                }
                WaitSplash waitSplash = new WaitSplash();
                waitSplash.execute();

                if(getRootView() instanceof SplashActivity) {
                    ((SplashActivity) getRootView()).startRootActivity();
                    ((SplashActivity) getRootView()).overridePendingTransition(R.anim.enter_pull_in, R.anim.exit_fade_out);
                }
            }



        }

        @Override
        public void onPasswordChanged() {
//            Log.d(TAG, "onPasswordChanged: "+getView() + getView().getPassword().toString());
            if (getView()!=null) {
                if (getView().getPassword().matches(ConstantManager.PATTERN_PASSWORD)) {
                    getView().setAcceptablePassword();
                }
                else {
                    getView().setNonAcceptablePassword();
                }

            }
        }

        @Override
        public void onEmailChanged() {
            if (getView()!=null) {
                if (getView().getEmail().matches(ConstantManager.PATTERN_EMAIL))
                    getView().setAcceptableEmail();
                else
                    getView().setNonAcceptableEmail();
            }
        }

        @Override
        public boolean checkUserAuth() {
            return mAuthModel.isAuthUser();
        }

        @Override
        public void onLoginSuccess() {
            if (getView()!=null && getRootView()!=null) {
                getRootView().showMessage(getView().getContext().getString(R.string.authentificate_successful));
                getView().hideLoginBtn();
                getView().showIdleWithAnim();
            }
        }

        @Override
        public void onLoginError(String message) {
            if (getRootView()!=null) {
                getRootView ().showMessage(message);
            }
        }
    }

    //endregion
}
